package anagram;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {                              //override, overload
    public static void main(String[] args) {
        //MARKOTNY -> ROMANTYK
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj dwa słowa, sprawdzę czy są anagramami");

        System.out.println("Podaj pierwszę słowo");
        String firstWord = in.nextLine();
        System.out.println("Podaj drugie słowo");
        String secondWord = in.nextLine();


        char[] firstChars = firstWord.toCharArray();
        char[] secondChars = secondWord.toCharArray();

        Arrays.sort(firstChars);
        Arrays.sort(secondChars);

        boolean equals = Arrays.equals(firstChars, secondChars);
        if (equals) {
            System.out.println("Słowa są anagramami");
        }
        else {
            System.out.println("Słowa nie są anagramami");
        }

    }
}
