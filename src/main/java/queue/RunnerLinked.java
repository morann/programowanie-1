package queue;

import java.util.LinkedList;

public class RunnerLinked {
    public static void main(String[] args) {
        LinkedQueue<Integer> queue = new LinkedQueue<>();
        queue.offer(40);
        queue.offer(10);
        queue.offer(20);
        queue.offer(5);
        queue.pool();           // ściągam 40,
        queue.offer(15);
        queue.offerFirst(1);
        System.out.println("peekFirst(): " + queue.peek());
        System.out.println(queue.size());
        System.out.println("peekLast(): " + queue.peekLast());

        //java.util.LinkedList
        LinkedList<String> connect = new LinkedList<>();
        connect.offer("Something");
        connect.offer("looks");
        connect.offer("visible");
        System.out.println(connect.peek());
        System.out.println(connect.peekFirst());
        System.out.println(connect.peekLast());
        connect.offerLast("to me");
        connect.offerFirst("I thought");
        System.out.println(connect.peekFirst());
        connect.pollFirst();
        System.out.println(connect.peekFirst());
        System.out.println(connect.size());

    }
}
