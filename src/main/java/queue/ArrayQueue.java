package queue;

import java.util.Arrays;

public class ArrayQueue<T> implements Queue<T> {

    private Object[] elements;

    public ArrayQueue() {
        this.elements = new Object[0];
    }

    // kopia tablicy, której wielkość zawsze size + 1
    // []
    // [] <- 145
    // [] -> copyOf(stara tablica, rozmiar starej + 1)
    // [ ,] -> elements[]
    // Arrays.copyOf()elements.length - 1] -> 145
    //[145]
    // elements[elements.length - 1] = element
    @Override
    public void offer(T element) {
        elements = Arrays.copyOf(elements, elements.length + 1);
        elements[elements.length - 1] = element;
    }

    @Override
    public T pool() {
        if (checkIfArrayIsEmpty()) return null;
        // Pobranie pierwszego elementu
        elements = Arrays.copyOfRange(elements, 1, elements.length);
        return (T) elements[0];
    }

    // DRY don't repeat yourself
    // SOLID
    @Override
    public T peek() {
        if (checkIfArrayIsEmpty()) return null;
        return (T) elements[0];
    }

    @Override
    public int size() {
        return elements.length;
    }

    private boolean checkIfArrayIsEmpty() {
        if (elements.length == 0) {
            System.out.println("Queue is empty");
            return true;
        }
        return false;
    }
}
