package queue;

public class LinkedQueue<T> implements Queue<T> {

    // Single LinkedList -> next
    // Double LinkedList <- -> previous/ next

    // Czoło naszej kolejki
    private Link<T> head;

    @Override           // Single -> Double previous
    // czy mamy czoło kolejki
    // [LINK, LINK, LINK] <- offer 50
    // [LINK -> daj następnegp, LINK-> daj następnegp, LINK ...., 50]
    // -> offerLast()
    public void offer(T element) {
        Link<T> newLink = new Link<>(element);
        if (head == null) {
            head = newLink;
            // jeśli nie mamy czoła kolejki to je ustawiamy
        } else {
            // mamy czoła
            // dopóki badane ogniwo ma kolejny element
            // idziemy do "ogona"
            Link<T> link = this.head;
            while (link.getNext() != null) {
                // iterujemy aż dojdziemy do ogona
                link = link.getNext();
            }
            link.setNext(newLink);
        }
    }

    public void offerFirst(T element) {
        Link<T> newLink = new Link<>(element);
        if (head == null) {
            head = newLink;
        } else {
            Link<T> link = this.head;
            while (link.getPrevious() != null) {
                link = link.getPrevious();
            }
            link.setPrevious(newLink);
            head = newLink;
            head.setNext(link);
            System.out.println(newLink.getNext());
        }
    }

    @Override
    public T pool() {
        if (head == null) {
            System.out.println("Empty queue");
            return null;
        }
        // jeżeli jest, pobieramy wartość head
        T headValue = head.getValue();
        head = head.getNext();
        return headValue;
    }

    // -> peekFirst()
    @Override
    public T peek() {
        return (T) head.getValue();
    }

    // -> peekLast()
    public T peekLast() {
        if (head == null) {
            System.out.println("Empty queue");
            return null;
        } else if (head.getNext() == null) {
            return head.getValue();
        } else {
            Link<T> link = this.head;
            while (link.getNext() != null) {
                link = link.getNext();
            }
            return link.getValue();
        }
    }

    @Override       // zaimplementuj
    public int size() {
        int count = 1;
        if (head == null) {
            return 0;
        } else if (head != null) {
            Link<T> link = this.head;
            while (link.getNext() != null) {
                link = link.getNext();
                count++;
            }
            return count;
        }
        return 0;
    }
}
