package queue;

public class Link<T> {
    private T value;
    private Link<T> next;
    private Link<T> previous;

    public Link<T> getPrevious() {
        return previous;
    }

    public void setPrevious(Link<T> previous) {
        this.previous = previous;
    }

    public Link<T> getNext() {
        return next;
    }

    public void setNext(Link<T> next) {
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public Link(T value) {
        this.value = value;
    }
}

//     _____________________
//    |    ____________     |
//    |   | T    |Link |    |
//    |   |value |next |    |
//    |   |____________|    |
//    |_____________________|