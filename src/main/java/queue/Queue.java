package queue;

public interface Queue<T>{

    void offer(T element);

    T pool();

    T peek();

    int size();

}

//    default int offe(int element) {
//        return 2 + element;
//    }
//    static int of(int element) {
//        return 2;
//    }


// co to jest zmienna (miejsce w pamięci), klasa,
// interfejs (kontrakt - metody, klasa implementująca musi go spełnić)
// klasa abstrakcyjna a interfejs do javy 8 w abstr mogły być zaimplementowane metody