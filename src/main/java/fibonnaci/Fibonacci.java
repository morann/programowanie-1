package fibonnaci;

import java.util.Scanner;

public class Fibonacci {  // F n = F n-1 + F n-2    1, 1, 2, 3, 5, 8

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Podaj indeks liczby z ciągu Fibonacciego");
        int number = in.nextInt();
        System.out.println(fibonacci(number));
    }
    static int fibonacci (int n) {
        if (n >= 3) {
            return fibonacci(n -1) + fibonacci(n - 2);
        }
        else {
            return 1;
        }
    }
}
