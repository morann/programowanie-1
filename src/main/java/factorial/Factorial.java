package factorial;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        // 4 ! -> 4 * 3 * 2 * 1
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę dla której wyliczysz silnię");
        int number = in.nextInt();
        System.out.println(factorial(number));

    }
    static int factorial (int n) {                 //rekurencja
        if (n >= 1) {
            return n * factorial(n-1);
        }
        else {return 1;}
    }
}
