package playground;

import java.util.Scanner;

public class FrogPlayground {

    private static int possibilities;

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("How many rocks do you see?");
        int stone = in.nextInt();
        System.out.println(calculatePossibilities(stone));

    }
    // Jump through one stone
    static int calculatePossibilities(int n) {
        if (n == 0) {
            return possibilities;
        } else {
            possibilities++;
            return calculatePossibilities(n - 1);
        }
    }
    static int calculateDifferentJumps (int m) {
        int jumpOneStone = 1;
        int jumpTwoStones = 2;
        if (m <= 1) {
            return possibilities++;
        }
        else {
            possibilities++;
            return calculatePossibilities(m - 1);
        }
    }
}
