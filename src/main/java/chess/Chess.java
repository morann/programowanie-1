package chess;

import java.util.Scanner;

public class Chess {

    public static final String black = " \u25A0 ";      // refactor this = control + t!!!
    public static final String white = " \u25A1 ";

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in); // command + p
        // command + option + v

        System.out.println("Podaj długość szachownicy");

        int chessLength = in.nextInt();

        System.out.println("Podaj szerokość szachownicy");

        int chessWidth = in.nextInt();

        System.out.println("Rysujesz szachownicę o długości " + chessLength + " i szerokości " + chessWidth);


        for (int y = 0; y < chessLength; y++) {

            for (int x = 0; x < chessWidth; x++) {

                if ((x + y) % 2 == 0) {
                    System.out.print(black);
                } else {
                    System.out.print(white);
                }
            }
            System.out.println("");
        }

    }
}
