package calculator;


public enum Example {
    RED("czerwony"){
        @Override
        public void something() {
            System.out.println("Something red");
        }
    },
    GREEN("zielony"){
        @Override
        public void something() {
            System.out.println("Something green");
        }
    },
    BLUE("niebieski"){
        @Override
        public void something() {
            System.out.println("Something blue");
        }
    };

    private String description;

    Example(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public abstract void something();


}