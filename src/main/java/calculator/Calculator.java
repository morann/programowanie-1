package calculator;

import java.util.function.IntBinaryOperator;

public enum Calculator {
    ADD("Dodawanie", new IntBinaryOperator() {
        @Override
        public int applyAsInt(int left, int right) {
            return left + right;
        }
    }),
    SUBTRACT("Odejmowanie", (x, y) -> x - y),
    MULTIPLY("Mnożenie", (x, y) -> x * y);

    private String description;
    private IntBinaryOperator operator;

    Calculator(String description, IntBinaryOperator operator) {
        this.description = description;
        this.operator = operator;
    }

    public int calculate(int a, int b) {
        return operator.applyAsInt(a, b);
    }
}
