package calculator;

public class Main {
    public static void main(String[] args) {
        Example.RED.something();
        Example.GREEN.something();
        Example.BLUE.something();

        System.out.println(Example.RED.getDescription());
        System.out.println(Example.GREEN.getDescription());
        System.out.println(Example.BLUE.getDescription());

        System.out.println(Calculator.ADD.calculate(3,6));
        System.out.println(Calculator.SUBTRACT.calculate(0,3));
        System.out.println(Calculator.MULTIPLY.calculate(-3, -6));
    }
}
