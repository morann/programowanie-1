package palindrom;

import java.util.Scanner;

public class Palindrom {                        //Kobyła ma mały bok
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Write your word");
        String word = in.nextLine();

        boolean isPalindrom = true;
        int start = 0;
        int end = word.length() - 1;

        while (start <= end) {
            System.out.println("Checking equality " + word.charAt(start) + " vs " + word.charAt(end));
            if (word.charAt(start) != word.charAt(end)) {
                isPalindrom = false;
                break;
            }
            start++;
            end--;
        }
        if (isPalindrom) {
            System.out.println("This is a palindrom.");
        }
        else {
            System.out.println("This is not a palindrom.");
        }
    }
}
