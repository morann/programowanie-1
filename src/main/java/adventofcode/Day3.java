package adventofcode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day3 {

    public static List<String> loadPath(Path path) {
        try {
            List<String> strings = Files.readAllLines(path);
            return strings;
        } catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }

    public static List<Point> findCommonPoints(List<Segment> first, List<Segment> second) {

        List<Point> commonPoints = new ArrayList<>();
        for (int i = 0; i < first.size() && i < second.size(); i++) {
            for (int j = 0; j < first.size() && j < second.size(); j++) {
                List<Point> commonPoint = first.get(i).commonPoints(second.get(j));
                commonPoint.stream()
                        .forEach(commonPoints::add);
            }
        }
        return commonPoints;
    }

    public static Map<Point, Integer> lowestIntersection(List<Segment> wireSegments, List<Point> commonPoints) {
        Map<Point, Integer> pointToWireLength = new HashMap<>();
        Integer wireLength = 0;
        Point lastTurnOfWire = new Point(0, 0);

        for (int i = 0; i < wireSegments.size(); i++) {             // for each segment
            Segment segment = wireSegments.get(i);
            if (segment.isHorizontal()) {
                lastTurnOfWire.y = segment.y1;                      // get lastTurnOfWire to calculate lengthToCross
                if (i == 0) {
                    lastTurnOfWire.x = 0;
                } else
                    lastTurnOfWire.x = wireSegments.get(i - 1).x1;
            } else if (segment.isVertical()) {
                lastTurnOfWire.x = segment.x1;
                if (i == 0) {
                    lastTurnOfWire.x = 0;
                } else
                    lastTurnOfWire.y = wireSegments.get(i - 1).y1;
            }

            for (int j = 0; j < commonPoints.size(); j++) {         // check cross with each common point
                Point point = commonPoints.get(j);
                if (segment.isVertical() && segment.x1 == point.x && point.y >= segment.y1 && point.y <= segment.y2) {
                    int lengthToCross = Math.abs(point.y - lastTurnOfWire.y);
                    pointToWireLength.put(point, lengthToCross + wireLength);
                } else if (segment.isHorizontal() && segment.y1 == point.y && point.x >= segment.x1 && point.x <= segment.x2) {
                    int lengthToCross = Math.abs(point.x - lastTurnOfWire.x);
                    pointToWireLength.put(point, lengthToCross + wireLength);
                }
            }
            wireLength = wireLength + segment.length();                 // lengthen wire
        }
        return pointToWireLength;
    }

    public static void main(String[] args) {

        Path input = Path.of("src/main/resources/adventofcode/day3input.txt");

        List<String> AOC_input = loadPath(input);

//        System.out.println(AOC_input.get(0));
//        System.out.println(AOC_input.get(1));

        String[] splitAOC_firstInput = AOC_input.get(0).split(",");
        String[] splitAOC_secondInput = AOC_input.get(1).split(",");

        List<Segment> segmentsFirstWire = Segment.createSegments(splitAOC_firstInput);
        List<Segment> segmentsSecondWire = Segment.createSegments(splitAOC_secondInput);

        List<Point> commonPoints = findCommonPoints(segmentsFirstWire, segmentsSecondWire);
        commonPoints.forEach(point -> System.out.println(point.x + ", " + point.y));
        commonPoints.remove(0);

        Optional<Integer> min = commonPoints.stream()
                .map(point -> Math.abs(point.x) + Math.abs(point.y))
                .min(Comparator.comparing(Integer::intValue));
        System.out.println(min);

        Map<Point, Integer> pointIntegerMap1 = lowestIntersection(segmentsFirstWire, commonPoints);
        Map<Point, Integer> pointIntegerMap2 = lowestIntersection(segmentsSecondWire, commonPoints);

        System.out.println("Map 1: " + pointIntegerMap1.size());
        for (Map.Entry<Point, Integer> entry : pointIntegerMap1.entrySet()) {
            Point key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println("(" + key.x + ", " + key.y + ") -> " + value);
        }
        System.out.println("Map 2: " + pointIntegerMap2.size());
        for (Map.Entry<Point, Integer> entry : pointIntegerMap2.entrySet()) {
            Point key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println("(" + key.x + ", " + key.y + ") -> " + value);
        }
        Map<Point, List<Integer>> resultMap = Stream.concat(pointIntegerMap1.entrySet().stream(), pointIntegerMap2.entrySet().stream())
                .collect(Collectors.groupingBy(Map.Entry::getKey,
                        Collectors.mapping(Map.Entry::getValue, Collectors.toList())));

        System.out.println("Map result: " + resultMap.size());
        for (Map.Entry<Point, List<Integer>> entry : resultMap.entrySet()) {
            Point key = entry.getKey();
            List<Integer> value = entry.getValue();
            System.out.println("(" + key.x + ", " + key.y + ") -> " + (value.get(0) + value.get(1)));
        }

        Optional<Integer> minWireLength = resultMap.values().stream()
                .map(list -> list.get(0) + list.get(1))
                .min(Comparator.comparing(Integer::intValue));
        System.out.println(minWireLength);

    }
}

