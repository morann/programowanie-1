package adventofcode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Day2 {

    public static String loadPath(Path path) {
        try {
            String numbers = Files.readString(path);
            return numbers;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Integer> mapToInt(String[] empArray) {
        List<Integer> intInput = new ArrayList<>();
        for (String string : empArray) {
            int i = Integer.parseInt(string);
            intInput.add(i);
        }
        return intInput;
    }

    public static List<Integer> opcode(List<Integer> list) {

        for (int i = 0; i < list.size(); i += 4) {

            if (list.get(i) == 1) {
                Integer first = list.get(i + 1);
                Integer second = list.get(i + 2);
                int addInt = list.get(first) + list.get(second);
                int changeIntOnPosition = list.get(i + 3);
                list.set(changeIntOnPosition, addInt);

            } else if (list.get(i) == 2) {
                Integer first = list.get(i + 1);
                Integer second = list.get(i + 2);
                int multiplyInt = list.get(first) * list.get(second);
                int changeIntOnPosition = list.get(i + 3);
                list.set(changeIntOnPosition, multiplyInt);

            } else if (list.get(i) == 99) {
                return list;
            }
        }
        return list;
    }

    public static int result(List<Integer> list) {
        ArrayList<Integer> copyList = new ArrayList<>(list);
        for (int noun = 0; noun < 100; noun++) {

            for (int verb = 0; verb < 100; verb++) {
                copyList.set(1, noun);
                copyList.set(2, verb);
                opcode(copyList);
                if (copyList.get(0) == 19690720) {
                    return 100 * noun + verb;
                } else if (copyList.get(0) != 19690720) {
                    Collections.copy(copyList, list);
                }
            }
        }
        return 0;
    }
//        StackOverflowError
//        ArrayList<Integer> dest = new ArrayList<>(list);
//        Random random = new Random();
//        int noun = random.nextInt(100);
//        int verb = random.nextInt(100);
//        dest.set(1, noun);
//        dest.set(2, verb);
//        opcode(dest);
//        if (dest.get(0) != 19690720) {
//            result(list);}
//        else if (dest.get(0) == 19690720) {
//            return 100 * noun + verb;}
//        return 100 * noun + verb;

    public static void main(String[] args) {

        Path input = Path.of("src/main/resources/adventofcode/day2input.txt");

        String stringInput = loadPath(input);
        String[] splitString = stringInput.split(",");
        List<Integer> intInput = mapToInt(splitString);
        System.out.println(intInput);
        // First star
//        intInput.set(1, 12);
//        intInput.set(2, 2);
//        System.out.println(intInput);
//        System.out.println(opcode(intInput));
        // Second star
        System.out.println(result(intInput));
    }
}
