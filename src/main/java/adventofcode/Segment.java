package adventofcode;

import java.util.ArrayList;
import java.util.List;

public class Segment {

    int x1;
    int x2;
    int y1;
    int y2;

    public Segment(int x1, int x2, int y1, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    @Override
    public String toString() {
        return "Segment{" +
                "x1=" + x1 + ", x2=" + x2 + ", y1=" + y1 + ", y2=" + y2 + '}';
    }

    boolean isVertical() {
        return x1 == x2;
    }

    boolean isHorizontal() {
        return y1 == y2;
    }

    int length() {
        if (this.isHorizontal()) {
            return Math.abs(this.x2 - this.x1);
        } else if (this.isVertical()) {
            return Math.abs(this.y2 - this.y1);
        }
        return 0;
    }

    public static List<Segment> createSegments(String[] input) {

        Integer coordinate_x = 0;
        Integer coordinate_y = 0;
        List<Segment> segments = new ArrayList<>();

        for (int i = 0; i < input.length; i++) {

            if (String.valueOf(input[i].charAt(0)).equals("U")) {

                segments.add(new Segment(coordinate_x, coordinate_x,
                        coordinate_y, coordinate_y + Integer.parseInt(input[i].substring(1))));
                coordinate_y = coordinate_y + Integer.parseInt(input[i].substring(1));

            } else if (String.valueOf(input[i].charAt(0)).equals("D")) {

                segments.add(new Segment(coordinate_x, coordinate_x,
                        coordinate_y - Integer.parseInt(input[i].substring(1)), coordinate_y));
                coordinate_y = coordinate_y - Integer.parseInt(input[i].substring(1));

            } else if (String.valueOf(input[i].charAt(0)).equals("R")) {

                segments.add(new Segment(coordinate_x, coordinate_x + Integer.parseInt(input[i].substring(1)),
                        coordinate_y, coordinate_y));
                coordinate_x = coordinate_x + Integer.parseInt(input[i].substring(1));

            } else if (String.valueOf(input[i].charAt(0)).equals("L")) {

                segments.add(new Segment(coordinate_x - Integer.parseInt(input[i].substring(1)), coordinate_x,
                        coordinate_y, coordinate_y));
                coordinate_x = coordinate_x - Integer.parseInt(input[i].substring(1));
            }
        }
        return segments;
    }
//                          ______
//       y2        _______        |
//                |       |____|__|______
//       y1  _____|            |
//          x1    x2

    public List<Point> commonPoints(Segment other) {

        List<Point> commonPoints = new ArrayList<>();

        if (this.isVertical() && other.isHorizontal()) {
            if (this.x1 >= other.x1 && this.x1 <= other.x2 && other.y1 >= this.y1 && other.y1 <= this.y2) {
                commonPoints.add(new Point(this.x1, other.y1));
            }
        } else if (this.isHorizontal() && other.isVertical()) {
            if (other.x1 >= this.x1 && other.x1 <= this.x2 && this.y1 >= other.y1 && this.y1 <= other.y2) {
                commonPoints.add(new Point(other.x1, this.y1));
            }
        } else if (this.isHorizontal() && other.isHorizontal() && this.y1 == other.y1) {
            if (this.x1 >= other.x1) {
                for (int i = 0; i <= other.length(); i++) {
                    if (other.x1 + i == this.x1) {
                        commonPoints.add(new Point(this.x1, this.y1));
                    }
                }
            } else if (this.x1 < other.x1) {
                for (int i = 0; i <= other.length(); i++) {
                    if (this.x1 + i == other.x1) {
                        commonPoints.add(new Point(other.x1, this.y1));
                    }
                }
            }

        } else if (this.isVertical() && other.isVertical() && this.x1 == other.x1) {
            if (this.y1 >= other.y1) {
                for (int i = 0; i <= other.length(); i++) {
                    if (other.y1 + i == this.y1) {
                        commonPoints.add(new Point(this.x1, this.y1));
                    }
                }
            } else if (this.y1 < other.y1) {
                for (int i = 0; i <= other.length(); i++) {
                    if (this.y1 + i == other.y1) {
                        commonPoints.add(new Point(this.x1, other.y1));
                    }
                }
            }
        }
        return commonPoints;
    }

}



