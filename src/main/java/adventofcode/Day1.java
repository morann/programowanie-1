package adventofcode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Day1 {
    // mass / 3 -> round down -> -2
    public static void main(String[] args) throws IOException {

        Path input = Paths.get("src/main/resources/adventofcode/day1input.txt");
        List<String> listOfModulesMass = Files.readAllLines(input);
        System.out.println(listOfModulesMass);
        int sum = 0;
        for (String mass : listOfModulesMass) {
            int fullRequirment = calculateFuel(Integer.parseInt(mass));
            System.out.println("Fuel needed for " + mass + " -> " + fullRequirment);
            sum += fullRequirment;
        }
        System.out.println("Sum of required fuel: " + sum);

    }

    public static int calculateFuel(int mass) {
        int fuel = mass / 3 - 2;
        if (fuel <= 0) {
            return 0;
        } else return fuel + calculateFuel(fuel);
    }
}
