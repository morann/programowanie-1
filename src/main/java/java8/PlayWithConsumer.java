package java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class PlayWithConsumer {
    public static void main(String[] args) {

        Consumer<String> consumer = (String t) -> System.out.println(t);          //command + kliknięcie
        Consumer<String> consumer2 = System.out::println;

        consumer.accept("Something very wise");                                // immutable!!! niemutowalny

        List<String> names = new ArrayList<>(Arrays.asList("Kasia", "Ania", "Basia"));

        Consumer<List<String>> listConsumer = (List<String> t) -> t.clear();
        Consumer<List<String>> listConsumer2 = List::clear;
        listConsumer.accept(names);
        System.out.println(names.size());

        names
                .forEach(consumer);

        MyConsumer<List<String>> addConsumer = (List<String> t) -> t.add("Hello");
        MyConsumer<List<String>> secondAddConsumer = (List<String> t) -> t.add("Hi");
        MyConsumer<List<String>> groupingConsumer = addConsumer.andThen(secondAddConsumer);
        groupingConsumer.accept(names);
        System.out.println(names);

    }
}