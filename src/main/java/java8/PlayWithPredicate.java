package java8;

import java.util.function.Predicate;

public class PlayWithPredicate {
    public static void main(String[] args) {

        Predicate<String> checkA = (String t) -> t.contains("A");
        System.out.println(checkA.test("Ann"));
        System.out.println(checkA.test("Sam"));

        Predicate<String> isEmpty = (String::isEmpty);
        System.out.println(isEmpty.test(""));
        System.out.println(isEmpty.test("Something inside"));

        Predicate<String> isAAndIsEmpty = checkA.and(isEmpty);
        System.out.println(isAAndIsEmpty.test("test"));
    }
}
