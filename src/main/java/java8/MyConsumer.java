package java8;


import java.util.Objects;

@FunctionalInterface
public interface MyConsumer<T> {

    void accept(T t);                                       //1 metoda abstrakcyjna,
                                                            // pozostałe defaultowe implementują ją
    default MyConsumer<T> andThen(MyConsumer<T> other){
        Objects.requireNonNull(other);
        return (T t) -> {
            this.accept(t);
            other.accept(t);
        };
    }
}
