package lock;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Podaj kombinacje do zamknięcia zamka w formacie X-X-X");
        Scanner scanner = new Scanner(System.in);
        String[] lockCombination = scanner.nextLine().split("-");

        int first = Integer.parseInt(lockCombination[0]);
        int second = Integer.parseInt(lockCombination[1]);
        int third = Integer.parseInt(lockCombination[2]);

        Lock lock = new Lock(first, second, third);

        System.out.println("Tworzę zamek lock -> " + lock);

        lock.shuffle();
        System.out.println(lock);
        lock.switchA();
        System.out.println(lock);
        lock.switchC();
        System.out.println(lock);
        System.out.println(lock.isOpen());

    }
}
