package stack;

public class Stack<T> {
    private T array[];
    private  int top;
    private int capacity;

    //Stack stack = new Stack(10);
    //stack.push/pop
    public Stack(int capacity) {
        this.capacity = capacity;
        this.array = (T[]) new Object[capacity];
        this.top = -1;
    }
    private boolean isFull() {
        return top == capacity - 1;
    }
    private boolean isEmpty() {
        return top == - 1;
    }

    public void push(T element) {
        if (isFull()) {
            System.out.println("Stack is full");
        }
        else
        array[++top] = element;
    }

    public T pop() {
        if (isEmpty()) {
            System.out.println("Stack is empty");
        }
        else
        return array[top--];
        return null;
    }

    public T peek() {
        return array[top];
    }


}
