package stack;

import java.util.Stack;

public class RunnerStack {
    public static void main(String[] args) {

        // java.util.Stack
        Stack<Integer> integers = new Stack<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        System.out.println("Created stack");
        integers.forEach(integer -> System.out.println(integer));
        System.out.println("Peek watch " + integers.peek().intValue());
        integers.forEach(integer -> System.out.println(integer));
        System.out.println("Pop remove " + integers.pop().intValue());
        integers.forEach(integer -> System.out.println(integer));
        System.out.println("Push add to the top");
        System.out.println(integers.push(1).intValue());
        System.out.println(integers.push(2).intValue());
        System.out.println(integers.push(3).intValue());
        System.out.println(integers.push(4).intValue());
        System.out.println(integers.capacity());
        integers.push(5);
        integers.add(0, 0);
        integers.push(6);
        integers.push(7);
        System.out.println(integers.capacity());
        System.out.println("Size " + integers.size());
        integers.forEach(integer -> System.out.println(integer));

        // Created Stack
        stack.Stack createdStack = new stack.Stack(10);
        createdStack.push("My ");
        createdStack.push("creation ");
        createdStack.push("of ");
        createdStack.push("stack... ");
        System.out.println(createdStack.peek());
        System.out.println(createdStack.pop());
        System.out.println(createdStack.peek());
        createdStack.push("stack... ");
        System.out.println(createdStack.peek());
    }

}
